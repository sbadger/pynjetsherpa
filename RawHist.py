import sys
import re
import numpy as np
import cStringIO
from math import sqrt
import subprocess
import glob

class RawHist:

    @staticmethod
    def latex(hn, head, *hlist):
        s = r"""
\begin{table}\centering
  \begin{tabular}{ccc}
    %% %s
    \hline \\
    %s
    \hline
""" % (hn, head)
        totalxs = [0.] * len(hlist)
        for i in range(len(hlist[0].hdata[hn][0])):
            (x1,x2,y,yp,ym) = hlist[0].hdata[hn][...,i]
            if x2 < 0:
                s += "   $%.1f$ --- $(%.1f)$" % (x1, x2)
            else:
                s += "   $%.1f$ --- $%.1f$" % (x1, x2)
            width = x2 - x1
            for hi in range(len(hlist)):
                (nx1,nx2,y,yp,ym) = hlist[hi].hdata[hn][...,i]
                assert x1 == nx1 and x2 == nx2
                val, err = y*width, 0.5*(yp+ym)*width
                totalxs[hi] += val
                if 0.1 < err:
                    s += " & $%.1f$ $(%.1f)$" % (val, err)
                elif 0.01 < err:
                    s += " & $%.2f$ $(%.2f)$" % (val, err)
                elif 0.001 < err:
                    s += " & $%.3f$ $(%.3f)$" % (val, err)
                elif 0.0001 < err:
                    s += " & $%.4f$ $(%.4f)$" % (val, err)
                elif 0.00001 < err:
                    s += " & $%.5f$ $(%.5f)$" % (val, err)
            s += " \\\\\n"
        s += r"""\hline
  %% total XS = %s
  \end{tabular}
\end{table}
""" % ', '.join("%f" % xs for xs in totalxs)
        return s

    def clone(self):
        new = RawHist()
        for hn in self.hdata.keys():
            new.hdata[hn] = np.array(self.hdata[hn])
        return new

    def align(self, names, edges, merge=1, relative=False):
        for hn,(low,high) in zip(names, edges):
            hdhn = self.hdata[hn]
            if low not in hdhn[0]:
                if low < hdhn[0][0]:
                    raise RuntimeError("stretching left unimplemented")
                fi = len([x for x in hdhn[0] if x < low])-1
                if fi != 0:
                    continue
                ti = fi + merge
                newwidth = hdhn[1][ti]-low
                if relative:
                    newwidth = hdhn[1][ti]-hdhn[0][fi]
                area = sum((hdhn[1][i]-hdhn[0][i])*hdhn[2][i] for i in range(fi,ti+1))
                errarea1 = sum((hdhn[1][i]-hdhn[0][i])*hdhn[3][i] for i in range(fi,ti+1))
                errarea2 = sum((hdhn[1][i]-hdhn[0][i])*hdhn[4][i] for i in range(fi,ti+1))
                newheight = area/newwidth
                hdhn[0][ti] = low
                hdhn[2][ti] = area/newwidth
                hdhn[3][ti] = errarea1/newwidth
                hdhn[4][ti] = errarea2/newwidth
                self.hdata[hn] = np.delete(hdhn, range(0, ti), 1)
        return None

    @staticmethod
    def dictalign(histdict, names, edges, merge=1, relative=False):
        newdict = {}
        for k,hist in histdict.items():
            assert isinstance(hist, RawHist)
            newdict[k] = hist.clone()
            newdict[k].align(names, edges, merge, relative)
        return newdict

    @staticmethod
    def listalign(histlist, names, edges, merge=1, relative=False):
        newlist = []
        for k in range(len(histlist)):
            assert isinstance(histlist[k], RawHist)
            newlist.append(histlist[k].clone())
            newlist[k].align(names, edges, merge, relative)
        return newlist

    @staticmethod
    def summed(histlist):
        new = RawHist()
        for hn in histlist[0].hdata.keys():
            new.hdata[hn] = np.array(histlist[0].hdata[hn])
            new.hdata[hn][2] = sum(histlist[i].hdata[hn][2] for i in range(len(histlist)))
            new.hdata[hn][3] = np.sqrt(sum(np.square(histlist[i].hdata[hn][3]) for i in range(len(histlist))))
            new.hdata[hn][4] = np.sqrt(sum(np.square(histlist[i].hdata[hn][4]) for i in range(len(histlist))))
        return new

    @staticmethod
    def sqsummed(histlist):
        new = RawHist()
        for hn in histlist[0].hdata.keys():
            new.hdata[hn] = np.array(histlist[0].hdata[hn])
            new.hdata[hn][2] = sum(histlist[i].hdata[hn][2]**2 for i in range(len(histlist)))
            new.hdata[hn][3] = np.array([0]*len(new.hdata[hn][0]))
            new.hdata[hn][4] = np.array([0]*len(new.hdata[hn][0]))
        return new

    @staticmethod
    def averaged(histlist):
        return RawHist.summed(histlist)/len(histlist)

    @staticmethod
    def openfile(fname):
        if fname.endswith('.aida'):
            pipe = subprocess.Popen(["aida2flat", fname], stdout=subprocess.PIPE).stdout
            return RawHist(pipe)
        else:
            with open(fname, 'r') as f:
                return RawHist(f)

    @staticmethod
    def extractfile(tar, fname):
        f = tar.extractfile(fname)
        rh = RawHist(f)
        f.close()
        return rh

    def __init__(self, f=None):
        self.count = 0
        self.mmax = 0
        self.hdata = {}
        self.htitle = {}
        self.xlow = {}
        self.xmax = {}
        self.nbin = {}
        if f:
            self.fromfile(f)

    def fromfile(self, f):
        state = 0
        for line in f:
            if state == 0 and line[:18] == '# BEGIN HISTOGRAM ':
                m = re.match(r"^/([^/]+)/(.*)$", line[18:])
                if m:
                    analysis, histname = m.group(1), m.group(2)
                else:
                    raise RuntimeError("Can't not parse histogram name")
                sio = cStringIO.StringIO()
                nbins = 0
                state = 1
            elif state == 1:
                if line[0] != '#':
                    pass
                else:
                    state = 2
            elif state == 2:
                if line[0] == '#':
                    m = re.match(r"^## Num bins: (\d+)$", line)
                    if m:
                        nbins = int(m.group(1))
                else:
                    sio.write(line)
                    state = 3
            elif state == 3:
                if line[0] != '#':
                    sio.write(line)
                else:
                    sio.seek(0)
                    self.hdata[histname] = np.loadtxt(sio, unpack=True)
                    if len(self.hdata[histname].shape) == 1:
                      self.hdata[histname] = np.array([[x] for x in self.hdata[histname]])
                    assert len(self.hdata[histname][0]) == nbins
                    sio = None
                    state = 0
        f.close()

    def __add__(self, other):
        new = RawHist()
        for hn in self.hdata.keys():
            assert (self.hdata[hn][:2] == other.hdata[hn][:2]).all()
            new.hdata[hn] = np.array(self.hdata[hn])
            (y0, eu0, ed0) = self.hdata[hn][2:]
            (y1, eu1, ed1) = other.hdata[hn][2:]
            new.hdata[hn][2] = y0 + y1
            new.hdata[hn][3] = np.sqrt(eu0*eu0 + eu1*eu1)
            new.hdata[hn][4] = np.sqrt(ed0*ed0 + ed1*ed1)
        return new

    def __sub__(self, other):
        new = RawHist()
        for hn in self.hdata.keys():
            assert (self.hdata[hn][:2] == other.hdata[hn][:2]).all()
            new.hdata[hn] = np.array(self.hdata[hn])
            (y0, eu0, ed0) = self.hdata[hn][2:]
            (y1, eu1, ed1) = other.hdata[hn][2:]
            new.hdata[hn][2] = y0 - y1
            new.hdata[hn][3] = np.sqrt(eu0*eu0 + eu1*eu1)
            new.hdata[hn][4] = np.sqrt(ed0*ed0 + ed1*ed1)
        return new

    def __mul__(self, other):
        new = RawHist()
        for hn in self.hdata.keys():
            new.hdata[hn] = np.array(self.hdata[hn])
            new.hdata[hn][2] = self.hdata[hn][2] * other
            new.hdata[hn][3] = self.hdata[hn][3] * other
            new.hdata[hn][4] = self.hdata[hn][4] * other
        return new

    def square(self):
        new = RawHist()
        for hn in self.hdata.keys():
            new.hdata[hn] = np.array(self.hdata[hn])
            new.hdata[hn][2] = self.hdata[hn][2]**2
            new.hdata[hn][3] = self.hdata[hn][3]**2
            new.hdata[hn][4] = self.hdata[hn][4]**2
        return new

    def sqrt(self):
        new = RawHist()
        for hn in self.hdata.keys():
            new.hdata[hn] = np.array(self.hdata[hn])
            new.hdata[hn][2] = np.sqrt(self.hdata[hn][2])
            new.hdata[hn][3] = np.sqrt(self.hdata[hn][3])
            new.hdata[hn][4] = np.sqrt(self.hdata[hn][4])
        return new

    def __div__(self, other):
        new = RawHist()
        if isinstance(other, RawHist):
            for hn in self.hdata.keys():
                assert (self.hdata[hn][:2] == other.hdata[hn][:2]).all()
                new.hdata[hn] = np.array(self.hdata[hn])
                (y0, eu0, ed0) = self.hdata[hn][2:]
                (y1, eu1, ed1) = other.hdata[hn][2:]
                new.hdata[hn][2] = y0/y1
                new.hdata[hn][3] = np.sqrt(eu0*eu0*y1*y1 + eu1*eu1*y0*y0)/(y1*y1)
                new.hdata[hn][4] = np.sqrt(ed0*ed0*y1*y1 + ed1*ed1*y0*y0)/(y1*y1)
        else:
            for hn in self.hdata.keys():
                new.hdata[hn] = np.array(self.hdata[hn])
                new.hdata[hn][2] = self.hdata[hn][2] / other
                new.hdata[hn][3] = self.hdata[hn][3] / other
                new.hdata[hn][4] = self.hdata[hn][4] / other
        return new

    def merge(self, hn, pairs):
        for (a,b) in pairs:
            val = 0.
            err1 = 0.
            err2 = 0.
            for i in range(a, a+b):
                width = self.hdata[hn][1][i]-self.hdata[hn][0][i]
                val += width*self.hdata[hn][2][i]
                tmp = width*self.hdata[hn][3][i]
                err1 += tmp*tmp
                tmp = width*self.hdata[hn][4][i]
                err2 += tmp*tmp
            err1 = sqrt(err1)/b
            err2 = sqrt(err2)/b

            self.hdata[hn][1][a] = self.hdata[hn][1][a+b-1]
            width = self.hdata[hn][1][a]-self.hdata[hn][0][a]
            self.hdata[hn][2][a] = val/width
            self.hdata[hn][3][a] = err1/width
            self.hdata[hn][4][a] = err2/width
            self.hdata[hn] = np.delete(self.hdata[hn], range(a+1, a+b), 1)


