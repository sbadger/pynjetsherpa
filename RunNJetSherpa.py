#!/usr/bin/python
from NJetSherpa import *
from RawHist import *
import getopt
import sys
import re
import platform
from os.path import exists
from os import environ
from os.path import splitext
from subprocess import call

from njetsherpa_config import *

def MakeCardsProcess(params):
  for part in params.nloparts:
    ppart = re.split("\_", part)

    card = readcard(params.templatefile)

    card[1]['run'] = editcmd(card[1]['run'], 'EVENTS', '0')
    card[1]['processes'] = editcmd(card[1]['processes'], 'NLO_QCD_Part', ppart[0])
    card[1]['integration'] = editcmd(card[1]['integration'], 'ERROR', '5.0')
    card[1]['integration'] = editcmd(card[1]['integration'], 'Finish_Optimization', 'Off')

    if (ppart[0]=='I' or ppart[0] =='RS'):
      card[1][''] = addcmd(card[1]['run'], 'DIPOLE_ALPHA %s;' % params.dipolealpha)

    if ppart[0]=='V':
      card[1]['run'] = addcmd(card[1]['run'], 'USE_DUMMY_VIRTUAL=1;')
      card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'Loop_Generator Internal;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')

    if ppart[0]=='RS':
#      card[1]['integration'] = addcmd(card[1]['integration'], 'RS_INTEGRATOR 7;')
      if params.splitRS==1:
        if not(ppart[1]==''):
          card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
          card[1]['processes'] = addcmd(card[1]['processes'], 'Min_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'Max_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')

    writecard(card, 'NJ-Run%s-process.dat' % part)

def MakeCardsGrid(params):
  for part in params.nloparts:
    card = readcard(params.templatefile)
    ppart = re.split("\_", part)

    card[1]['run'] = editcmd(card[1]['run'], 'EVENTS', '0')
    if part[0]=='V' and params.splitV:
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + ppart[0])
    elif ppart[0]=='RS' or ppart[0]=='I':
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + part + '_' + params.dipolealpha)
    else:
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + part)
    card[1]['processes'] = editcmd(card[1]['processes'], 'NLO_QCD_Part', ppart[0])
    card[1]['integration'] = editcmd(card[1]['integration'], 'ERROR', '0.5')
    card[1]['integration'] = editcmd(card[1]['integration'], 'Finish_Optimization', 'On')

    if (ppart[0]=='I' or ppart[0] =='RS'):
      card[1][''] = addcmd(card[1]['run'], 'DIPOLE_ALPHA %s;' % params.dipolealpha)

    if ppart[0]=='V':
      card[1]['run'] = addcmd(card[1]['run'], 'USE_DUMMY_VIRTUAL=1;')
      card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'Loop_Generator Internal;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')

    if ppart[0]=='RS':
#      card[1]['integration'] = addcmd(card[1]['integration'], 'RS_INTEGRATOR 7;')
      if params.splitRS==1:
        if not(ppart[1]==''):
          card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
          card[1]['processes'] = addcmd(card[1]['processes'], 'Min_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'Max_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')

    writecard(card, 'NJ-Run%s-grid.dat' % part)

def MakeCardsEvent(params):
  for part in params.nloparts:
    card = readcard(params.templatefile)
    ppart = re.split("\_", part)

    card[1]['run'] = editcmd(card[1]['run'], 'EVENTS', params.Nevts[part])
    if ppart[0]=='V' and params.splitV:
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + ppart[0])
    elif ppart[0]=='RS' or ppart[0]=='I':
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + part + '_' + params.dipolealpha)
    else:
      card[1]['run'] = addcmd(card[1]['run'], 'RESULT_DIRECTORY=Result/' + part)
    card[1]['processes'] = editcmd(card[1]['processes'], 'NLO_QCD_Part', ppart[0])
    card[1]['integration'] = editcmd(card[1]['integration'], 'ERROR', '0.5')
    card[1]['integration'] = editcmd(card[1]['integration'], 'Finish_Optimization', 'On')

    if (ppart[0]=='I' or ppart[0] =='RS'):
      card[1][''] = addcmd(card[1]['run'], 'DIPOLE_ALPHA %s;' % params.dipolealpha)

    if ppart[0]=='V':
      card[1]['run'] = addcmd(card[1]['run'], 'SHERPA_LDADD SherpaLHOLE njet2;')
      card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'Loop_Generator LHOLE;')
      card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')
      if params.splitV:
        if ppart[1]=='L':
          card[1]['run'] = addcmd(card[1]['run'], 'LHOLE_CONTRACTFILE=OLE_contractL.lh')
        if ppart[1]=='SL':
          card[1]['run'] = addcmd(card[1]['run'], 'LHOLE_CONTRACTFILE=OLE_contractSL.lh')

    if ppart[0]=='RS':
#      card[1]['integration'] = addcmd(card[1]['integration'], 'RS_INTEGRATOR 7;')
      if params.splitRS==1:
        if not(ppart[1]==''):
          card[1]['processes'] = rmcmd(card[1]['processes'], '  End process;')
          card[1]['processes'] = addcmd(card[1]['processes'], 'Min_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'Max_N_Quarks %s;' % str(2*int(ppart[1])))
          card[1]['processes'] = addcmd(card[1]['processes'], 'End process;')

    writecard(card, 'NJ-Run%s-event.dat' % part)

def MakeCards(stage, params):
  if stage=='process':
    MakeCardsProcess(params)
  elif stage=='grid':
    MakeCardsGrid(params)
  elif stage=='event':
    MakeCardsEvent(params)
    ### generate and edit OLE_order here ###
    ### if OLE_order.lh doesn't exist ###
    if exists('OLE_order.lh'):
      print("# OLE_order.lh exists...editing")
      blha = readblha('OLE_order.lh')
      blha = editblha(blha, 'IRregularisation', 'CDR')
      blha = rmblha(blha, 'OperationMode')
      blha = replaceblha(blha, 'MatrixElementSquareType', 'AmplitudeType')
      blha = extrablha(blha, 'NJetPrintStats', 'yes')
      print("# Creating BLHA contract file for virtual matrix elements")
      if params.splitV:
        blha = editblha(blha, 'AmplitudeType', 'LeadingPart')
        writeblha(blha, "OLE_order-njetL.lh")
        blha = editblha(blha, 'IRregularisation', 'ZERO')
        blha = editblha(blha, 'AmplitudeType', 'SubLeadingPart')
        blha = extrablha(blha, 'NJetZeroIsPositive', 'yes')
        writeblha(blha, "OLE_order-njetSL.lh")
        call([params.njet2path+'/bin/njet.py', '-o','OLE_contractL.lh', 'OLE_order-njetL.lh'])
        call([params.njet2path+'/bin/njet.py', '-o','OLE_contractSL.lh', 'OLE_order-njetSL.lh'])
      else:
        blha = editblha(blha, 'AmplitudeType', 'Loop')
        writeblha(blha, "OLE_order-njet.lh")
        call([params.njet2path+'/bin/njet.py', '-o','OLE_contract.lh', 'OLE_order-njet.lh'])
    else:
      print("# Creating BLHA order file for virtual matrix elements")
      print("# wait for \"gauge check zero...\" to appear repeatedly")
      print("# then hit ctrl-c to abort the evaluation.")
      print("# you will then need to run RunNJetSherpa.py again.")
      raw_input("  hit any key to continue...")
      if params.splitV:
        call([params.sherpapath+'/bin/Sherpa'+params.sherpasuffix, '-g', '-f', 'NJ-RunV_L-event.dat', 'INIT_ONLY=1'])
      else:
        call([params.sherpapath+'/bin/Sherpa'+params.sherpasuffix, '-g', '-f', 'NJ-RunV-event.dat', 'INIT_ONLY=1'])

def readXS(njets, part):
  files = glob.glob('RivetAnalysis/%s/hist*.aida' % part)
  for f in files:
    fout = splitext(f)[0]+'.hist'
    call(['aida2flat','-o',fout,f])
  files = glob.glob('RivetAnalysis/%s/hist*.hist' % part)
  hists = map(RawHist.openfile, files)
  NLOpart = RawHist.summed(hists)/len(hists)
  return NLOpart

def usage():
    print """\
Usage: RunNJetSherpa.py [OPTION...] [FILE]

  -t, --template <filename> [default = 'Run_Template.dat']
  -C, --clean
  -n, --nocards
  -p, --process
  -g, --grids
  -e, --events
  -c, --cores <no. of cores>
  -N, --nloparts <part1:part2:...>        selection of NLO parts: B,V,I or RS
  -E, --nevent <part1:n1:part2:n2:...>    number of events for each NLO part
  -d, --dipolealpha                       dipole alpha for RS and I [default=0.05]
  -s, --seedstart <integer>                event sets generated with random seeds
                                          <seedstart> -> <seedstart> + <cores>
  -Q, --splitRS <max quark lines>         split RS by number of quark lines
  -L, --splitV                            split V by number leading/sub-leading colour
  -T, --eventtype=[root,histograms]       either generate root Ntuples or use sherpa analysis
                                          via Rivet or Internal analysis and save histograms
                                          choosing histograms requires correct analysis options
                                          in Run_Template.dat file. [default=root]
  -A, --analyzehistograms                 [still under development]

Other options:
  -h, --help                              show this help message
"""

class Params:
  def __init__(self):
    try:
      opts, args = getopt.getopt(sys.argv[1:],
          "ti:hCnpgec:N:E:s:Q:Ld:,T:A",
          ["template=", "help", "clean", "nocards", "process", "grids", "events", "cores=",
            "nlopart=", "nevents=", "seedstart=", "splitRS=", "splitV", "dipolealpha=", "eventtype=", "analyzehistograms"])
    except getopt.GetoptError, err:
      print str(err)
      usage()
      sys.exit(2)

    self.templatefile = 'Run_Template.dat'
    self.clean = 0
    self.usempi = 0
    self.cores = 1
    self.makecards = 1
    self.runprocess = 0
    self.rungrids = 0
    self.eventtype = 'root'
    self.runevents = 0
    self.seedstart = 1
    self.nloparts = ['B', 'V', 'I', 'RS']
    self.Nevts = { 'B': 1000, 'V': 1000, 'I': 1000, 'RS': 10000}
    self.splitV = 0
    self.splitRS = 0
    self.maxquarklines = 0
    self.dipolealpha = '0.05'
    self.analyzehistograms = 0

    for op, oparg in opts:
      if op in ("-h", "--help"):
        usage()
        sys.exit()
      elif op in ("-t", "--template"):
        self.templatefile = oparg
      elif op in ("-C", "--clean"):
        self.clean = 1
      elif op in ("-n", "--nocards"):
        self.makecards = 0
      elif op in ("-p", "--process"):
        self.runprocess = 1
      elif op in ("-g", "--grids"):
        self.rungrids = 1
      elif op in ("-e", "--events"):
        self.runevents = 1
      elif op in ("-c", "--cores"):
        self.usempi = 1
        self.cores = int(oparg)
      elif op in ("-E", "--nevents"):
        elist = re.split(":", oparg)
        edict = {}
        for i in range(len(elist)/2):
          edict[elist[2*i]] = elist[2*i+1]
        for p in self.nloparts:
          if p in edict:
            self.Nevts[p] = edict[p]
          else:
            print('using default events for '+p)
      elif op in ("-s", "--seedstart"):
        self.seedstart = int(oparg)
      elif op in ("-L", "--splitV"):
        self.splitV = 1
        self.nloparts.remove('V')
        self.nloparts.extend(['V_L', 'V_SL'])
        self.Nevts['V_L'] = 1000
        self.Nevts['V_SL'] = 100
      elif op in ("-Q", "--splitRS"):
        self.maxquarklines = int(oparg)
        self.splitRS = 1
        self.nloparts.remove('RS')
        for i in range(self.maxquarklines+1):
          self.nloparts.append('RS_%d' % i)
          self.Nevts['RS_%d' % i] = 1000
      elif op in ("-d", "--dipolealpha"):
        self.dipolealpha = oparg
      elif op in ("-N", "--nloparts"):
        self.nloparts = re.split(":", oparg)
      elif op in ("-T", "--eventtype"):
        self.eventtype = oparg
      elif op in ("-A", "--analyzehistograms"):
        self.analyzehistograms = 1
      else:
        assert False, "unhandled option"

    self.njet2path = njet2path
    self.sherpapath = sherpapath
    self.sherpasuffix = sherpasuffix
    print(self.sherpapath)
    self.mpisherpapath = mpisherpapath

    if platform.system() == 'Linux':
      environ["LD_LIBRARY_PATH"] = environ["LD_LIBRARY_PATH"] + ':' + extralibpath
      environ["LD_LIBRARY_PATH"] = environ["LD_LIBRARY_PATH"] + ':' + self.njet2path + '/lib'
    elif platform.system() == 'Darwin':
      environ["DYLD_LIBRARY_PATH"] = environ["DYLD_LIBRARY_PATH"] + ':' + extralibpath
      environ["DYLD_LIBRARY_PATH"] = environ["DYLD_LIBRARY_PATH"] + ':' + self.njet2path + '/lib'
    else:
      print('WARNING: not set-up for this OS yet...please set paths manually')

def main():
  params = Params()

  if params.clean:
    call(['rm', '-rf', 'Process/', 'Result/'])
    call('rm NJ-Run*.dat OLE_*.lh Sherpa_References.tex', shell=True)
    sys.exit()

  if params.makecards:
    stages = ['process', 'grid', 'event']
    for s in stages:
      MakeCards(s, params)

  if params.runprocess:
    for part in params.nloparts:
      call(params.sherpapath+'/bin/Sherpa'+params.sherpasuffix+' -g -f NJ-Run'+part+'-process.dat >  process%s.log &' % part, shell=True)

  if params.rungrids:
    for part in params.nloparts:
      ppart = re.split("\_", part)
      if ppart[0]=='RS' or ppart[0]=='I':
        runname = part + '_' + params.dipolealpha
      else:
        runname = part
      if params.usempi:
        call('mpiexec -np '+str(params.cores)+' '+params.mpisherpapath+'/bin/Sherpa'+params.sherpasuffix+' -f NJ-Run'+part+'-grid.dat < /dev/null > grid%s.log' % runname, shell=True)
      else:
        call([params.sherpapath+'/bin/Sherpa'+params.sherpasuffix, '-f', 'NJ-Run'+part+'-grid.dat'])

  if params.runevents:
    if params.eventtype=='root':
      for p in params.nloparts:
        print('Running '+str(params.Nevts[p])+' '+p+' events')
      call(['mkdir','-p','Event'])
      for part in params.nloparts:
        ppart = re.split("\_", part)
        if ppart[0]=='RS' or ppart[0]=='I':
          runname = part + '_' + params.dipolealpha
        else:
          runname = part
        call(['mkdir','-p','Event/%s' % runname])
        ### this solution seems a bit dumb...still, 1st attempt
        script = open('mkevents.sh','w')
        script.write('#! /bin/sh \n')
        for c in range(params.seedstart,params.seedstart+params.cores):
          script.write(params.sherpapath+'/bin/Sherpa'+params.sherpasuffix+' -f NJ-Run'+part+'-event.dat -R '+str(c)+
            ' EVT_FILE_PATH=Event/'+runname+
            ' EVENT_OUTPUT=Root[event'+str(c)+'] > Event/'+runname+'/run'+str(c)+'.log 2>&1 & \n')
        script.write('wait')
        script.close()
        call(['chmod','a+x','mkevents.sh'])
        print("## starting "+part+" events")
        call(['./mkevents.sh'])
        call(['rm','./mkevents.sh'])
    elif params.eventtype=='histograms':
      for p in params.nloparts:
        print('Running '+str(params.Nevts[p])+' '+p+' events')
      call(['mkdir','-p','RivetAnalysis'])
      for part in params.nloparts:
        ppart = re.split("\_", part)
        if ppart[0]=='RS' or ppart[0]=='I':
          runname = part + '_' + params.dipolealpha
        else:
          runname = part
        call(['mkdir','-p','RivetAnalysis/%s' % runname])
        script = open('mkevents.sh','w')
        script.write('#! /bin/sh \n')
        for c in range(params.seedstart,params.seedstart+params.cores):
          script.write(params.sherpapath+'/bin/Sherpa'+params.sherpasuffix+' -f NJ-Run'+part+'-event.dat -R '+str(c)+
            ' ANALYSIS_OUTPUT=RivetAnalysis/'+runname+'/hist'+str(c)+
            ' > RivetAnalysis/'+runname+'/run'+str(c)+'.log 2>&1 & \n')
        script.write('wait')
        script.close()
        call(['chmod','a+x','mkevents.sh'])
        print("## starting "+part+" events")
        call(['./mkevents.sh'])
        call(['rm','./mkevents.sh'])

  if params.analyzehistograms:
      NLOXS = {};
      for part in params.nloparts:
        ppart = re.split("\_", part)
        if ppart[0]=='RS' or ppart[0]=='I':
          runname = part + '_' + params.dipolealpha
        else:
          runname = part

        print('Attempting to read aida histograms from RivetAnalysis/'+runname)
        NLOXS[part] = readXS(2, runname)
        xs = NLOXS[part].hdata['XS']
        print "XS (",part,") = ", xs[2,0],"(",xs[3,0],") pb"

      NLO = RawHist.summed([NLOXS[p] for p in params.nloparts ])
      xs = NLO.hdata['XS']
      print "Total XS (NLO) = ", xs[2,0],"(",xs[3,0],") pb"

if __name__ == '__main__':
    main()
